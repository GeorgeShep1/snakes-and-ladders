# Snakes and Ladders  
This project is an investigation into the statistics of snakes and ladders, Sample contains the code for initializing the board and moving forward in time. It also contains various files for visualizing and calculating properties associated with snakes and ladders.  

As expected, Tests contains all tests. Scripts contains one file responsible for running all tests associated with the project.  

# Installing project:
This project can be cloned by running:  
`git clone https://GeorgeShep1@bitbucket.org/GeorgeShep1/snakes-and-ladders.git`  

# Modules
The Pipfile contains all modules required, if these are already installed then many of these sections can be skipped.  
This project runs on Python version 3.6.7, and will not run on earlier versions due to type annotations.  

# Installing pipenv
`sudo pip install pipenv`  

# Installing dependencies
1. Change directory into project folder:  
> e.g. `cd ~/Documents/{foldername}`
2. Install dependencies after installing pipenv:  
> `pipenv install`

# Adding a dependency
> `pipenv install {dependency}`

# Removing a dependency
> `pipenv uninstall {dependency}`

# Running project:
Way 1:
Run file directly
> `pipenv run python {filename}`  
  
Way 2:  
Spawn a shell with access to packages and then run like normal  
> `pipenv shell`  
> `python {filename}`  

Way 3:
Install packages in Pipfile and then run through normal terminal
> `pip3 install {package}`  
> `python {filename}`  

# Running tests:
1. Navigate to root folder  
> e.g. `cd ~Documents/{foldername}`
2. Run test script  
> `python scripts/test.py`  
Alternatively:  
> `make -f Makefile test`  
will run all tests.

# Writing tests:
Add 'test_' prefix to all test files

# Autoformatting:
1. Install autopep8:  
> `pip3 install pycodestyle`  
> `pip3 install --upgrade autopep8`  
2. Run Makefile command:
> `make -f Makefile format`  

Note: 1. can be skipped if being ran in pipenv shell discussed in Running project

# Description of Files

## Sample/Game  
The Game folder contains several folders that implement the logic behind the game, all investigations into properties of snakes and ladders come from this folder. create_jumps deals with the creation of a variable number of snakes and ladders such that they don't conflict with the specified rules of the game. default_snakes_ladders contains a dictionary of the default snakes and ladders to use which is used extensively for comparison between different methods. Finally, game_setup deals with the creation of the transition matrix given start and end conditions, while also being responsible for iterating the game forwards in time.

## Sample  
The remaining .py files present in the top level of the Sample directory are dedicated to files that actually investigate the properties of the game.

### average_jump
This investigates if there is a correlation between the total net increase/decrease caused by the
snakes and ladders, on the average number of moves till victory from the
start square

### board_plot
This creates a plot of a board given a set of snakes and ladders and the size of the board.

### comparison_end
This compares the PDF for a setup of snakes and ladders for each possible end condition, it can also compare the start conditions as well.

### dice_size
The file investigates the affect the dice size has on the finishing time of the game, while comparing again the net movement of the snakes and ladders.

### dist_ordering_squares
This uses the probability density function to calculate the moves till finish and the variance associated with them.

### entropy
This investigates the entropy of the game as a function of the number of moves taken.

### finish_probability
This creates a plot of the pdf and cdf as a function of the number of moves.

### format_graphs
Nothing important, contains a function to format graphs to look the same.

### math_ordering_squares
Uses the transition matrix to calculate the number of moves till victory and the variance associated with this.

### monte_ordering_squares
Uses Monte-Carlo simulation to calculate the number of moves till victory and the variance.

### probability_heat_plot
Shows how the probability distribution varies for a small board as the number of moves increases.

### shortest_path
Calculates the shortest path from start to finish for a board and shows this on a plot.

### transition_matrix
Creates a heatmap showing a representation of the transition matrix.

