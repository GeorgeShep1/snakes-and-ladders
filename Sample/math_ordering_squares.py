# This calculates the ordering of the board and the standard
# deviation/variance using the properties of the transition matrix

from Game.game_setup import Game
from Game.create_jumps import generate_points
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from Game.default_snakes_ladders import snakes, ladders
from format_graphs import format_graph


def double_flip(matrix: np.ndarray) -> np.ndarray:
    """
    Flips a matrix upside down and left to right
    """
    return matrix[::-1, ::-1]


def expectation(transition: np.ndarray) -> [[float], [float]]:
    """
    Calculates average time till victory from each square on board and the
    standard deviation on this value from the properties of the transition matrix
    Args:
        transition: matrix representing transition matrix for this board
    Returns:
        T_w is a list of the expected time till victory from each square
        S is the standard deviation on each of these values
    """
    # Flip matrix so it matches notation given
    flipped = double_flip(transition)
    # Extract Q and R matrices
    Q = flipped[1:, 1:]
    R = flipped[1:, 0]
    # Enforce correct shape
    R.shape = (len(Q[0]), 1)

    # Calculate F as given in notation
    F = np.linalg.inv((np.identity(len(Q[1])) - Q))
    T_w = np.sum(F, axis=1)
    # p_k = np.dot(F, R)

    # Calculate variance and standard deviation
    V = np.dot(2 * F - np.identity(len(F[0])), T_w) - np.square(T_w)
    S = np.sqrt(V)
    return T_w, S


if __name__ == "__main__":
    # General board properties
    start_condition = 1
    size = 100
    squares = list(range(start_condition, size, 1))

    # Choice of snakes and ladders
    snakes, ladders = snakes, ladders
    # snakes, ladders = generate_points(squares, 0, 0)

    # Set up of Game instance and calculation of transition matrix
    game = Game(size, start_condition=start_condition, end_condition='Over')
    game.transition_matrix(squares, snakes, ladders)

    # Calculate time till win and deviation for this transition matrix
    T_w, S = expectation(game.transition)

    # print('Mathematical values')
    for values in zip(range(start_condition, size), T_w[::-1], S[::-1]):
        print('{:.0f}, {:.2f}, {:.2f}'.format(*values))

    plt.style.use('ggplot')
    fig, ax = plt.subplots()
    ax = format_graph(ax)
    plt.title(
        f'Average moves till victory starting from each square \n on a board of size {size}')
    plt.xlabel('Start tile')
    plt.ylabel('Average moves till victory')

    plt.plot(range(start_condition, size), T_w[::-1])
    plt.show()
    fig.savefig('Sample/Figures/MovesTillVictoryMath.eps', format='eps')
