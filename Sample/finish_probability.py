# This illustrates the pdf and cdf for a random board setup

import numpy as np
from Game.default_snakes_ladders import snakes, ladders
from Game.game_setup import Game
from format_graphs import format_graph
import matplotlib.pyplot as plt

# Choice of snakes and ladders
# snakes, ladders = {}, {}
snakes, ladders = snakes, ladders

# General board properties
start_condition = 1
size = 100
squares = range(start_condition, size + 1)

# Set up of game instance
end_condition = 'Bounce'
game = Game(size, start_condition=start_condition, end_condition=end_condition)
game.transition_matrix(squares, snakes, ladders)

cdf = []
moves = 80
for i in range(1, moves + 1):
    # cdf of finishing after n moves starting from first square
    probability = np.linalg.matrix_power(game.transition, i)[0, -1]
    cdf.append(probability)

# First value should be zero so pdf is calculated correctly
cdf = np.insert(cdf, 0, 0)
pdf = np.diff(cdf)

plt.style.use('ggplot')
fig, ax = plt.subplots(
    2, 1, figsize=(
        8, 6), gridspec_kw={
            'height_ratios': [
                1, 1]}, sharex=True)
ax[0] = format_graph(ax[0])

plt.title(f"CDF and PDF for board size {size}")
plt.ylabel('Cumulative probability \n of finishing')
plt.plot(range(0, moves + 1), cdf)

ax[1] = format_graph(ax[1])
plt.plot(range(1, moves + 1), pdf)
plt.xlabel('Number of moves')
plt.ylabel('Probability of \n finishing at turn n')
plt.show()
fig.savefig(f"Sample/Figures/cdfpdf{end_condition}.eps", format='eps')
