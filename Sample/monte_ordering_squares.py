# Ordering of squares and deviation on these values calculated using a
# monte carlo approach

import numpy as np
from Game.create_jumps import generate_points
from Game.default_snakes_ladders import snakes, ladders
import matplotlib.pyplot as plt
from format_graphs import format_graph

# General board properties
start_condition = 1
size = 100
squares = list(range(start_condition, size, 1))

# Choice of snakes and ladders
# snakes, ladders = generate_points(squares, 0, 0)
snakes, ladders = snakes, ladders

# Same functions as used in game_setup


def round_end(landing_square):
    return size


def exact_only(landing_square):
    return landing_square


def bounce_back(landing_square):
    return landing_square - (2 * (landing_square - size))


end_conditions = {
    'Bounce': bounce_back,
    'Exact': exact_only,
    'Over': round_end
}

# More setup of board and number of tests used to determine averages
end_condition = end_conditions['Over']
dice_size = 4
num_tests = 10000
moves_matrix = np.zeros((len(squares), num_tests))


combined_dict = {**snakes, **ladders}
for j, square in enumerate(squares):
    for i, _ in enumerate(range(num_tests)):
        # Start at a square on board and set moves to 0
        position = square
        number_of_moves = 0

        # Keep moving till at end of baord
        while position != size:
            number_of_moves += 1
            position += np.random.randint(1, dice_size + 1)

            # Factor in end conditions
            if position >= size:
                position = end_condition(position)

            # Check for snakes and ladders
            while position in combined_dict.keys():
                position = combined_dict[position]

        # Add number of moves till finish to row of results for same square
        moves_matrix[j, i] = number_of_moves

np.set_printoptions(precision=2)
# Calculate properties
expectation = np.mean(moves_matrix, axis=1)
expectation_2 = np.mean(np.square(moves_matrix), axis=1)
variance = expectation_2 - np.square(expectation)
deviation = np.sqrt(variance)
print('Monte-Carlo values')
for values in zip(squares, expectation, deviation):
    print('{:.0f}, {:.2f}, {:.2f}'.format(*values))

plt.style.use('ggplot')
fig, ax = plt.subplots()
ax = format_graph(ax)

plt.title('Average moves till victory starting from each square \n on a board of size {}'.format(size))
plt.xlabel('Start tile')
plt.ylabel('Average moves till victory')
plt.plot(squares, expectation)
plt.show()
fig.savefig('Sample/Figures/MovesTillVictoryMonte.eps', format='eps')
