# This uses the probability density function to compute the true ordering
# of the squares, and the variance/standard deviation

from Game.game_setup import Game
from Game.create_jumps import generate_points
import numpy as np
import matplotlib.pyplot as plt
from Game.default_snakes_ladders import snakes, ladders
from format_graphs import format_graph

# General board conditions
start_condition = 1
size = 100
squares = list(range(start_condition, size, 1))

# Choices for snakes and ladders
# snakes, ladders = {}, {}
snakes, ladders = snakes, ladders
# snakes, ladders = generate_points(squares, 8, 8)

# Game set up
game = Game(size, start_condition=start_condition, end_condition='Over')
game.transition_matrix(squares, snakes, ladders)

# Enough moves such that probability of finishing is as close to 1 as possible
n_moves = size * 2
# True board size
if start_condition == 0:
    board_size = size + 1
else:
    board_size = size

# Empty numpy array for cumulative probability of finishing on x-axis, after number of moves on y-axis
# e.g. probability of starting at square 1 and being finished by turn 5 is
# stored in index [0, 4] if start_condition = 1
cdf = np.zeros((n_moves + 1, board_size - 1))
for i in range(1, n_moves + 1):
    # Probability of finishing represented by transition to a power
    transition_power = np.linalg.matrix_power(game.transition, i)
    # Don't require probability of final square being finished as it is
    # already finished
    cdf[i, :] = transition_power[:-1, -1]
# First row should be zero such that pdf is calculated correctly on next line
cdf[0, :] = 0
pdf = np.diff(cdf, axis=0)

tiles = []
expectations = []

for i in range(0, len(pdf[0])):
    # For each column representing each square calculated properties
    x = np.arange(1, n_moves + 1)
    expectation = np.sum(x * pdf[:, i])
    expectation_2 = np.sum(x**2 * pdf[:, i])
    variance = expectation_2 - expectation**2
    print(i, '{:.2f}'.format(expectation), '{:.2f}'.format(np.sqrt(variance)))
    tiles.append(i)
    expectations.append(expectation)

plt.style.use('ggplot')
fig, ax = plt.subplots()
ax = format_graph(ax)

plt.title('Average moves till victory starting from each square \n on a board of size {}'.format(size))
plt.xlabel('Start tile')
plt.ylabel('Average moves till victory')
plt.plot(tiles, expectations, label='Moves till victory from starting square')
plt.legend(loc='best')
plt.show()
fig.savefig('Sample/Figures/MovesTillVictoryPdf.eps', format='eps')
