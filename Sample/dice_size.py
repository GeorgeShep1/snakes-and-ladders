# This investigates the correlation between the size of the dice and the expected finish time of the game
# Also looks at how the net movement from snakes and ladders affects this

import numpy as np
import matplotlib.pyplot as plt
from Game.game_setup import Game
from Game.create_jumps import generate_points
from math_ordering_squares import expectation
from Game.default_snakes_ladders import snakes, ladders
from format_graphs import format_graph

# General parameters for board
board_size = 100
start_condition = 0
squares = range(start_condition, board_size + 1)

# Set up plot
plt.style.use('ggplot')
fig, ax = plt.subplots()
ax = format_graph(ax)


snakes_ladders_list = [[snakes, ladders, 'Default'], [{}, {}, 'None']]
for snakes_ladders in snakes_ladders_list:
    dice_size, finish_time = [], []
    snakes, ladders, name = snakes_ladders
    # Dice size between 4 and 25
    for size in range(4, 26):
        # Set up instance of game
        game = Game(
            board_size,
            start_condition=start_condition,
            end_condition='Over',
            dice_size=size)
        transition = game.transition_matrix(squares, snakes, ladders)
        T_w, S = expectation(transition)
        # Add dice size and expected moves till finish
        dice_size.append(size)
        finish_time.append(T_w[-1])
        # Calculate net movement from snakes and ladders
        length = 0
        combined = {**snakes, **ladders}
        for key in combined.keys():
            length += combined[key] - key
    # Plot for specific set up of snakes and ladders
    plt.plot(dice_size, finish_time, label=name)

# for values in zip(dice_size, finish_time):
    # print(values)

plt.title('Correlation between dice size and finish time')
plt.ylabel('Expected moves till finish')
plt.xlabel('Dice size')
plt.legend(loc='best')
plt.show()
fig.savefig(f"Sample/Figures/dicesize.eps", format='eps')
