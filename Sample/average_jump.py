# This investigates if there is a correlation between the total net increase/decrease caused by the
# snakes and ladders, on the average number of moves till victory from the
# start square

import numpy as np
from math_ordering_squares import expectation
from Game.game_setup import Game
from Game.default_snakes_ladders import snakes, ladders
from Game.create_jumps import generate_points
import matplotlib.pyplot as plt
from format_graphs import format_graph

# Set up of board and conditions
board_size = 100
start_condition = 1
squares = range(start_condition, board_size + 1)
game = Game(board_size, start_condition=start_condition, end_condition='Over')

expectations, total_length = [], []
for _ in range(5):
    for snake_num in range(1, 24):
        for ladder_num in range(1, 24):
            snakes, ladders = generate_points(squares, snake_num, ladder_num)
            transition = game.transition_matrix(squares, snakes, ladders)
            try:
                # Calculate expected time till finish
                expectations.append(expectation(transition)[0][-1])
                # Calculate net movement from snakes and ladders
                # ladder from 12 to 20 is net movement 8
                length = 0
                combined = {**snakes, **ladders}
                for key in combined.keys():
                    length += combined[key] - key
                total_length.append(length)
            except BaseException:
                # Expectation requires inverting matrix, in some cases this is
                # not possible
                print("Singular matrix can't invert")

plt.style.use('ggplot')
fig, ax = plt.subplots()
ax = format_graph(ax)
plt.title('Correlation between total length of snakes minus \n ladders and finish time from starting square')
plt.xlabel('Snakes minus ladders')
plt.ylabel('Expected moves till finish')
plt.ylim([0, 100])
plt.scatter(total_length, expectations)
plt.show()
fig.savefig('Sample/Figures/NetMovement.eps', format='eps')
