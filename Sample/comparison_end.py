# This illustrates the pdf and cdf for the three seperate end conditions,
# Over, Bounce, and Exact

import numpy as np
from Game.default_snakes_ladders import snakes, ladders
from Game.game_setup import Game
from format_graphs import format_graph
import matplotlib.pyplot as plt
from Game.create_jumps import generate_points

# Choice of snakes and ladders
# snakes, ladders = {}, {}
# snakes, ladders = {}, {99: 1}
snakes, ladders = snakes, ladders

# General board properties

plt.style.use('ggplot')
fig, ax = plt.subplots()
ax = format_graph(ax)
plt.title(f"Comparison between PDFs \nfor each end condition")
plt.ylabel('Probability density function')
plt.xlabel('Number of moves')

end_conditions = ['Over', 'Bounce', 'Exact']
start_conditions = [1]
for end_condition in end_conditions:
    for start_condition in start_conditions:
        size = 100
        squares = range(start_condition, size + 1)

        # Set up of game instance
        game = Game(
            size,
            start_condition=start_condition,
            end_condition=end_condition)
        game.transition_matrix(squares, snakes, ladders)

        cdf = []
        moves = 100
        for i in range(1, moves + 1):
            # cdf of finishing after n moves starting from first square
            probability = np.linalg.matrix_power(game.transition, i)[0, -1]
            cdf.append(probability)

        # First value should be zero so pdf is calculated correctly
        cdf = np.insert(cdf, 0, 0)
        pdf = np.diff(cdf)
        if end_condition == 'Bounce':
            linestyle = '--'
            color = 'lime'
        if end_condition == 'Exact':
            linestyle = '-.'
            color = 'deeppink'
        if end_condition == 'Over':
            linestyle = '-'
            color = 'blue'
        plt.plot(range(1, moves + 1), pdf, label=f"{end_condition}", linestyle=linestyle, color=color)

plt.legend(loc='best')
plt.show()
# fig.savefig(f"Sample/Figures/cdfpdfcomparisonlongsnake.eps", format='eps')
fig.savefig(f"Sample/Figures/cdfpdfcomparison.eps", format='eps')
