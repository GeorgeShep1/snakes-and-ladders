# Set of default snakes and ladders created using create_jumps so
# different methods can be compared

snakes = {30: 19, 93: 70, 65: 22, 71: 64, 45: 21, 87: 45, 49: 16, 91: 40}
ladders = {10: 71, 7: 79, 70: 73, 25: 97, 69: 92, 26: 45, 11: 77, 16: 93}
