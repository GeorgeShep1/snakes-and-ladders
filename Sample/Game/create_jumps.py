# Used to create snakes and ladders to populate board with

import numpy as np
import random


def validate_create_request(
        squares: [int], num_snakes: int, num_ladders: int) -> bool:
    """
    Determines if number of snakes and ladders can be done
    Args:
        squares: list of squares on board
        num_snakes: number of snakes wanted
        num_ladders: number of ladders wanted
    Returns:
        boolean of whether it can be done or not
    """
    if (num_snakes < 0) or (num_ladders < 0):
        raise ValueError(
            'Zero or more amount of snakes and ladders are required')
    if ((num_snakes + num_ladders) * 2 > (len(squares) - 1)):
        raise ValueError('More snakes and ladders than possible')
    return True


def make_ladder(squares: [int], snakes: {
                int: int}, ladders: {int: int}) -> [int]:
    """
    Makes a ladder
    Args:
        squares: list of squares on board
        snakes: snakes that already exist
        ladders: ladders that already exist
    Returns:
        start and end of the new ladder
    """
    # Pick two random points
    points = random.sample(squares[1:], 2)
    ladder = sorted(points)
    # If points picked are not valid try again
    if ladder[0] in list(snakes.keys()):
        return make_ladder(squares, snakes, ladders)
    # Old playing rules
    # elif ladder[0] in list(snakes.values()):
        # return make_ladder(squares, snakes, ladders)
    # elif ladder[1] in list(snakes.keys()):
        # return make_ladder(squares, snakes, ladders)
    elif ladder[0] in ladders.keys():
        return make_ladder(squares, snakes, ladders)
    else:
        return ladder


def make_snake(squares: [int], snakes: {int: int}) -> [int]:
    """
    Makes a snake
    Args:
        squares: list of squares on board
        snakes: snakes that already exist
    Returns:
        start and end of snake
    """
    # Pick two random points
    points = random.sample(squares[:-1], 2)
    snake = sorted(points, reverse=True)
    # If points are not valid try again
    if snake[0] in snakes.keys():
        return make_snake(squares, snakes)
    else:
        return snake


def generate_points(squares: [int], num_snakes: int,
                    num_ladders: int) -> [{int: int}]:
    """
    Creates a set of snakes and ladders given board
    Args:
        squares: list of squares on board
        num_snakes: number of snakes wanted on board
        num_ladders: number of ladders wanted on board
    Returns:
        set of snakes and ladders
    """
    validate_create_request(squares, num_snakes, num_ladders)
    snakes, ladders = {}, {}
    for i in range(num_snakes):
        snake = make_snake(squares, snakes)
        # Take numbers from returned list and add to dictionary as key: start,
        # value: end
        snakes[snake[0]] = snake[1]
    for i in range(num_ladders):
        ladder = make_ladder(squares, snakes, ladders)
        # Same as for snakes
        ladders[ladder[0]] = ladder[1]

    if no_loops(snakes, ladders):
        return snakes, ladders
    else:
        return generate_points(squares, num_snakes, num_ladders)


def no_loops(snakes: {int: int}, ladders: {int: int}) -> bool:
    """
    Determines whether there are any infinite loops as these are disallowed, if travels down
    more than 10 snakes or ladders defined as a loop
    Args:
        snakes: snakes being used on board
        ladders: ladders being used on board
    Returns:
        true if no loops, false if not
    """
    combined = {**snakes, **ladders}
    keys = combined.keys()

    for square in keys:
        counter = 0
        while combined[square] in keys:
            counter += 1
            square = combined[square]
            if counter > 10:
                return False
    return True


if __name__ == "__main__":
    snakes, ladders = generate_points(range(1, 100), 8, 8)
    print(snakes, ladders)
    # # Are allowed but correspond to old playing rules
    # print(list(set(list(snakes.values())).intersection(list(ladders.values()))))
    print(list(set(list(snakes.keys())).intersection(list(ladders.values()))))
    print(list(set(list(snakes.values())).intersection(list(ladders.keys()))))
    # Should always be empty, shows start of ladder and start of snake not on
    # same square
    print(list(set(list(snakes.keys())).intersection(list(ladders.keys()))))
