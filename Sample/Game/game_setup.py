# Set up of Game class, specifies playing conditions such as start and end conditions,
# also is reponsible for creating transition matrix and iterating forwards
# steps

import numpy as np
import random


class Game():
    def __init__(self, size: int, **kwargs):
        """
        Sets up playing conditions
        Args:
            size: size of board
            end_conditions: optional - choice of 'Bounce', 'Exact', and 'Over' as to how the game is finished
                                      default: 'Over'
            dice_size: optional - size of the dice to use when rolled
                             default: 4
            start_condition: optional - whether starts off the board or on square 1
                             default: 1
        """
        end_conditions = {
            'Bounce': self.bounce_back,
            'Exact': self.exact_only,
            'Over': self.round_end
        }
        self.size = size
        self.dice = kwargs.get('dice_size', 4)

        end_condition = kwargs.get('end_condition', 'Over')
        self.end_condition = end_conditions[end_condition]

        self.start_condition = kwargs.get('start_condition', 1)
        self.position_vector = self.starting_vector()

    def create_empty_transition(self) -> np.ndarray:
        """
        Creates an empty matrix of correct size for use as a transition matrix
        Returns:
            Empty matrix
        """
        if self.start_condition == 1:
            return np.zeros((self.size, self.size))
        else:
            # 1 bigger if zero square is included
            return np.zeros((self.size + 1, self.size + 1))

    def transition_matrix(self, squares: [int], snakes: {
                          int: int}, ladders: {int: int}) -> np.ndarray:
        """
        Creates and fills in transition matrix given snakes, ladders, and squares on board
        Args:
            squares: list of squares on board
            snakes: snakes being placed on board
            ladders: ladders being placed on board
        Returns:
            transition matrix corresponding to board, snakes, and ladders
        """
        transition = self.create_empty_transition()
        # Once finished cannot go
        transition[-1, -1] = 1

        snakes_ladders = {**snakes, **ladders}
        snake_ladder_keys = snakes_ladders.keys()

        for square in squares:
            for i in range(1, self.dice + 1):
                # Cannot move when starting on final square
                if (square != self.size):
                    landing_square = i + square
                    # Refer to end condition if over board end
                    if landing_square > self.size:
                        landing_square = self.end_condition(
                            square, landing_square)
                    # Check position is valid
                    landing_square = self.check_position(
                        snake_ladder_keys, snakes_ladders, landing_square)
                    # Increase probability of current transition
                    transition[square - self.start_condition,
                               landing_square - self.start_condition] += 1 / self.dice

        self.transition = transition
        return transition

    def check_position(self, snake_ladder_keys: iter or list, snakes_ladders: {
                       int: int}, landing_square: int) -> int:
        """
        Recursive function that determines if current square is a valid position given snakes and ladders
        Args:
            snake_ladder_keys: starting points of both snakes and ladders
            snakes_ladders: combined snakes and ladders dictionary
            landing_square: square being checked to see if valid or not
        Returns:
            actual landing square after following any set of snakes or ladders
        """
        if landing_square in snake_ladder_keys:
            landing_square = snakes_ladders[landing_square]
        # Recursively check as chained snakes and ladders can exist
        if landing_square in snake_ladder_keys:
            return self.check_position(
                snake_ladder_keys, snakes_ladders, landing_square)
        return landing_square

    def starting_vector(self):
        """
        Creates vector corresponding to starting position
        Returns:
            vector with 1 in correct starting position
        """
        if self.start_condition == 1:
            position = np.zeros(self.size)
        else:
            position = np.zeros(self.size + 1)
        position[0] = 1
        return position

    def round_end(self, start_square: int, landing_square: int) -> int:
        """
        End condition of just needing to get on or past the finish
        Args:
            start_square: square started on this turn
            landing_square: landing square this turn
            NOTE: The above inputs are required in order to keep function generalized in transition matrix
        Returns:
            end position on the board
        """
        return self.size

    def exact_only(self, start_square: int, landing_square: int) -> int:
        """
        End condition of needing to finish exactly on end square otherwise returned to starting position
        of current turn
        Args:
            start_square: square started on this turn
            landing_square: landing square this turn
            NOTE: Again landing_square input required for generalized function
        Returns:
            start_square
        """
        return start_square

    def bounce_back(self, start_square: int, landing_square: int) -> int:
        """
        End condition of any excess moves causing the player to bounce back towards the start
        Args:
            start_square: square started on this turn
            landing_square: landing square on this turn
        Returns:
            True landing square after bounce back has been calculated
        """
        landing_square = landing_square - (2 * (landing_square - self.size))
        # For very small boards
        if landing_square <= 0:
            landing_square = 1
        return landing_square

    def move(self):
        """
        Method that moves position probability forward one step
        Returns:
            position vector iterated forwards by 1 turn
        """
        self.position_vector = np.dot(self.position_vector, self.transition)
        return self.position_vector
