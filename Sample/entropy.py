# This investigates the entropy of game with the number of moves

from scipy import stats
from format_graphs import format_graph
from Game.game_setup import Game
import matplotlib.pyplot as plt
import numpy as np
from Game.default_snakes_ladders import snakes, ladders
from Game.create_jumps import generate_points

# General board properties
start_condition = 1
board_size = 100
squares = range(start_condition, board_size + 1, 1)

# Generate snakes, ladders
# snakes, ladders = generate_points(squares, 24, 24)
snakes, ladders = snakes, ladders

# Set up instance of Game
game = Game(board_size, start_condition=1, end_condition='Over')
game.transition_matrix(squares, snakes, ladders)

# Number of turns to compute entropy for
turns = np.arange(101)
# Entropy as number of turns increases, calculated using sum p_i ln p_i
entropy = [
    stats.entropy(
        np.linalg.matrix_power(
            game.transition,
            turn)[0]) for turn in turns]

plt.style.use('ggplot')
fig, ax = plt.subplots()
ax = format_graph(ax)

plt.plot(turns, entropy)
plt.xlabel('Number of turns')
plt.ylabel('Entropy')
plt.title('Snakes and ladders entropy')
plt.show()
fig.savefig('Sample/Figures/Entropy.eps', format='eps')
