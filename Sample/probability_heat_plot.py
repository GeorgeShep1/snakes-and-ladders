# Illustrates how the probability of being at certain squares changes over time

import numpy as np
from Game.create_jumps import generate_points
from Game.game_setup import Game
import seaborn as sns
import matplotlib.pyplot as plt
from format_graphs import format_graph


def flip(transition):
    """
    Flips matrix upside down
    """
    return transition[::-1, :]


# Board properties
start_condition = 1
size = 9
squares = list(range(start_condition, size, 1))
# snakes, ladders = generate_points(squares, 1, 1)
snakes, ladders = {}, {5: 6}

# Set up of Game instance
game = Game(size, start_condition=start_condition)
game.transition_matrix(squares, snakes, ladders)

# Number of moves to plot up to
moves = 6
# Create matrix for storing probability of correct size
if start_condition == 1:
    probability_time = np.zeros((moves, size))
else:
    probability_time = np.zeros((moves, size + 1))

for i in range(moves):
    # Store probability vector as row in probability matrix
    probability_time[i, :] = game.move()

np.set_printoptions(precision=3)

xticks = np.arange(start_condition, size + 1, 1)

fig, ax = plt.subplots()
ax = format_graph(ax)

sns.set()
cmap = sns.cm.rocket_r
# Create heatmap using probability matrix
sns.heatmap(flip(probability_time),
            annot=True,
            fmt=".2f",
            linewidth=.5,
            xticklabels=xticks,
            yticklabels=np.arange(1, moves + 1)[::-1],
            cmap=cmap,
            cbar=False)
plt.title('Probabililty distribution as moves increases', fontsize=20)
plt.ylabel('Number of moves')
plt.xlabel('Square')
plt.show()
fig.savefig(f"Sample/Figures/illustrativecdf.eps", format='eps')
