# This illustrates the shortest path from start to finish and any
# intermediate stages of ladders and snakes

from scipy.sparse.csgraph import shortest_path
from Game.game_setup import Game
from Game.create_jumps import generate_points
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from board_plot import plot_arrow, draw_snake_ladder, square_to_coords
from Game.default_snakes_ladders import snakes, ladders

# General board properties
start_condition = 1
size = 100
squares = list(range(start_condition, size + 1, 1))
print('Snakes', snakes)
print('Ladders', ladders)

# Set up of Game instance
game = Game(size, start_condition=start_condition, end_condition='Over')
game.transition_matrix(squares, snakes, ladders)

# Calculate lengths which aren't used, and predecessors which states the square move must've come
# from for shortest path to finish
lengths, predecessors = shortest_path(game.transition, indices=0, directed=True,
                                      unweighted=True, return_predecessors=True)

# Modifies predecessors such that if board starts at 1 then first square is 1
if start_condition == 1:
    predecessors = [1 if i == 0 else i for i in predecessors]


path = [size - start_condition]
while path[0] > start_condition:
    path.insert(0, predecessors[path[0]])
path = [i + start_condition for i in path]
print('Shortest path', path)


# Calculate any intermediate stages in this shortest path
data = np.zeros(size)
temp_dict = {**snakes, **ladders}

for _ in range(5):
    # makes sure it catches up to 5 layers of chaining
    for values in temp_dict.keys():
        if temp_dict[values] in path:
            path.append(values)
path = set(path)

# Different value for intermediate stages, this will give different colour
# on heatmap
for value in path:
    if value in temp_dict.keys():
        data[value - 1] = 0.2
    else:
        data[value - 1] = 1

# Starting point added on
data[0] = 1

# Conform data and labels to right size then flip every second row to fit
# on board
side = int(np.sqrt(size))
data = data.reshape((side, side))
labels = np.zeros((side, side))
for i in range(len(labels)):
    start_index = side * i
    end_index = side * (i + 1)
    labels[i] = squares[start_index:end_index][::(-1)**i]
    data[i] = data[i][::(-1)**i]

fig, ax = plt.subplots()
# Create heatmap
sns.set()
cmap = sns.cm.rocket_r
ax = sns.heatmap(data,
                 #  linewidths=1,
                 cmap=cmap,
                 #  linecolor='Black',
                 cbar=False,
                 annot=labels,
                 fmt='.0f')

# Place arrows representing snakes and ladders
for g in snakes.keys():
    coords = draw_snake_ladder(g, snakes, labels)
    plot_arrow(coords, True)

for g in ladders.keys():
    coords = draw_snake_ladder(g, ladders, labels)
    plot_arrow(coords, False)

ax.invert_yaxis()
plt.xticks([])
plt.yticks([])
plt.show()
fig.savefig('Sample/Figures/ShortestPath.eps', format='eps')
