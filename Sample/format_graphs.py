# This is used for formatting of graphs such that they look the same

import matplotlib.pyplot as plt


def format_graph(ax):
    plt.sca(ax)

    plt.rcParams.update({'axes.titlesize': 18,
                         'legend.fontsize': 14,
                         'font.serif': 'Computer Modern Roman', })
    ax.yaxis.label.set_size(16)
    ax.xaxis.label.set_size(16)

    ax.spines['bottom'].set_color('black')
    ax.spines['top'].set_color('black')
    ax.spines['right'].set_color('black')
    ax.spines['left'].set_color('black')
    return ax
