# This illustrates the transition matrix

from Game.game_setup import Game
from Game.create_jumps import generate_points
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt


def flip(transition):
    """
    Flips matrix upside down
    """
    return transition[::-1, :]


# Some general conditions
start_condition = 1
size = 9
squares = list(range(start_condition, size, 1))

# Snakes and ladders generated
# snakes, ladders = generate_points(squares, 2, 2)
snakes, ladders = {}, {5: 6}
print('Snakes', snakes)
print('Ladders', ladders)

# Create game instance
game = Game(size, start_condition=start_condition)
game.transition_matrix(squares, snakes, ladders)

# Makes heatmap look better without many numbers
if (game.dice == 4):
    fmt = ".2f"
else:
    fmt = ".1f"

# x and y ticks for heatmap that correspond to tile number
xticks = np.arange(start_condition, size + 1, 1)
yticks = np.arange(start_condition, size + 1, 1)

flipped_matrix = flip(game.transition)
flipped_matrix = flip(flipped_matrix)
np.set_printoptions(precision=3)

fig, ax = plt.subplots()
sns.set(font_scale=1.5)
cmap = sns.cm.rocket_r
# Create heatmap showing transition matrix
ax = sns.heatmap(flipped_matrix,
                 annot=True,
                 fmt=fmt,
                 linewidths=.5,
                 xticklabels=xticks,
                 #  yticklabels=yticks[::-1],
                 cbar=False,
                 yticklabels=yticks,
                 cmap=cmap)
ax.tick_params(axis='y', rotation=0)
ax.xaxis.tick_top()
ax.yaxis.tick_left()
# plt.title('Probability of going from tile a to tile b, coords (b, a)')
plt.show()
fig.savefig('Sample/Figures/ExampleBoardTransitionMatrix.eps', format='eps')
