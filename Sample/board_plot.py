# This creates a heatmap that shows the probability distribution after a set number of moves, complete with a
# board that shows the snakes and ladders used

import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from Game.create_jumps import generate_points
from Game.game_setup import Game
from Game.default_snakes_ladders import snakes, ladders


def square_to_coords(square: int, labels: np.ndarray) -> [int]:
    """
    Calculates coordinate of specific square on board
    Args:
        square: square with coordinate that needs determining
        labels: 2d array that represents board
    Returns:
        x, y coordinate pair
    """
    index = np.where(labels == square)
    return index[0][0], index[1][0]


def draw_snake_ladder(start: int, snakes_ladders: {
                      int: int}, labels: np.ndarray) -> [int]:
    """
    Returns coordinates needed to plot arrow showing snake or ladder
    Args:
        start: start of snake or ladder
        snakes_ladders: all snakes and ladders placed on board
        labels: 2d array representing tiles on board
    Returns:
        start of arrow and how long arrow should be
    """
    y, x = square_to_coords(start, labels)
    y2, x2 = square_to_coords(snakes_ladders[start], labels)
    dx, dy = x2 - x, y2 - y
    return x, y, dx, dy


def plot_arrow(coords: [int], snake_ladder: bool):
    """
    Draws arrow on plot representing snake or ladder
    Args:
        coords: x, y, dx, dy used to create arrow
        snake_ladder: True for a snake, false for a ladder
    """
    x, y, dx, dy = coords
    if snake_ladder:
        color = 'red'
    else:
        color = 'green'
    plt.arrow(
        x + 0.5,
        y + 0.5,
        dx,
        dy,
        width=0.1,
        length_includes_head=True,
        color=color)


if __name__ == '__main__':
    # General conditions for game
    start_condition = 1
    board_size = 100
    squares = range(1, board_size + 1)

    # Choices for snakes and ladders
    snakes, ladders = {}, {}
    # snakes, ladders = {}, {5: 6}
    # snakes, ladders = snakes, ladders
    # snakes, ladders = generate_points(squares, 8, 8)

    # Size of one side of board
    side = int(np.sqrt(board_size))
    # Initial array for tiles on board
    labels = np.zeros((side, side))

    # Setup board with start_condition
    game = Game(board_size, start_condition=start_condition)
    game.transition_matrix(squares, snakes, ladders)

    # List representing probability of being at tiles
    if start_condition == 1:
        probability_time = np.zeros(board_size)
    else:
        probability_time = np.zeros(board_size + 1)

    # move moves times and place probability distribution into list, then
    # reshape into a square shape without flipping yet
    moves = 0
    for g in range(moves):
        game.move()
    probability_time = game.position_vector
    probability_time = np.reshape(probability_time, (side, side))

    # Flip every second row as snakes and ladders board curls round at edges
    for i in range(0, side):
        start_index = side * i
        end_index = side * (i + 1)
        labels[i] = squares[start_index:end_index][::(-1)**i]
        probability_time[i] = probability_time[i][::(-1)**i]

    fig, ax = plt.subplots()
    # Create heatmap, annotates with tiles, but underlying values used for heatmap are probabilities
    # Represents probability distribution after moves turns
    sns.set(font_scale=2)
    cmap = sns.cm.rocket_r
    ax = sns.heatmap(probability_time,
                     #   linewidths=1,
                     cmap=cmap,
                     #   linecolor='Black',
                     cbar=False,
                     annot=labels,
                     fmt='.0f')

    # Place arrows representing snakes and ladders
    for g in snakes.keys():
        coords = draw_snake_ladder(g, snakes, labels)
        plot_arrow(coords, True)

    for g in ladders.keys():
        coords = draw_snake_ladder(g, ladders, labels)
        plot_arrow(coords, False)

    ax.invert_yaxis()
    plt.xticks([])
    plt.yticks([])
    plt.show()
    fig.savefig('Sample/Figures/DefaultBoard.eps', format='eps')
