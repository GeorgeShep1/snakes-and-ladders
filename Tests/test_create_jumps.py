# Set of tests for the generation of the snakes and ladders
# Rules are as such:
# Negative amount of snakes or ladders cannot be created
# Too many snakes or ladders to fit on the board cannot be created
# No bottom of ladder can be at the head of a snake
# Cannot have a snake starting from where a snake already starts
# Cannot have a ladder starting from where a ladder already starts
# Chaining of snakes and ladders is allowed as long as each snake and
# ladder is valid

# There are a few xtest which are just tests that are skipped, these were designed to go with
# other rules which meant that snakes and ladders couldn't be chained together

from unittest import TestCase, mock, main
import unittest
import numpy as np
from Sample.Game.create_jumps import validate_create_request, generate_points, make_ladder, make_snake, no_loops
import inspect
import sys
import os

currentdir = os.path.dirname(
    os.path.abspath(
        inspect.getfile(
            inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)


class BasicTestSuite(unittest.TestCase):
    """Tests functionality of the generation of snakes and ladders"""

    def test_validate_request(self):
        """Checks that only a valid amount of snakes and ladders can be inputted"""
        squares = range(1, 100)
        with self.assertRaisesRegex(ValueError, 'Zero or more amount of snakes and ladders are required'):
            validate_create_request(squares, -1, 3)
        with self.assertRaisesRegex(ValueError, 'Zero or more amount of snakes and ladders are required'):
            validate_create_request(squares, 1, -3)
        with self.assertRaisesRegex(ValueError, 'More snakes and ladders than possible'):
            validate_create_request(squares, 25, 25)
        self.assertTrue(validate_create_request(squares, 1, 1))
        self.assertTrue(validate_create_request(squares, 0, 5))

    @mock.patch('random.sample')
    def test_make_ladder(self, choice_mock):
        """Tests creation of ladder"""
        choice_mock.return_value = [13, 8]
        ladder = make_ladder(range(1, 100), {}, {})
        self.assertEquals(ladder, [8, 13])

    @mock.patch('random.sample')
    def xtest_make_invalid_ladder(self, choice_mock):
        """Bottom of ladder can't be at bottom of snake"""
        choice_mock.side_effect = [[8, 13], [5, 2]]
        ladder = make_ladder(range(1, 100), {15: 8}, {})
        self.assertEquals(ladder, [2, 5])

    @mock.patch('random.sample')
    def test_make_invalid_ladder_2(self, choice_mock):
        """Bottom of ladder can't be at head of snake"""
        choice_mock.side_effect = [[8, 13], [5, 2]]
        ladder = make_ladder(range(1, 100), {8: 1}, {})
        self.assertEquals(ladder, [2, 5])

    # Old test for when ladders and snakes couldn't be chained together
    @mock.patch('random.sample')
    def xtest_make_invalid_ladder_3(self, choice_mock):
        """Top of ladder can't be at head of snake"""
        choice_mock.side_effect = [[8, 13], [5, 2]]
        ladder = make_ladder(range(1, 100), {13: 3}, {})
        self.assertEquals(ladder, [2, 5])

    # Old test that when chaining of snakes and ladders wasn't allowed
    @mock.patch('random.sample')
    def xtest_recursion_functions(self, choice_mock):
        """Given two invalid choices the third will be returned"""
        choice_mock.side_effect = [[8, 13], [5, 2], [15, 4]]
        ladder = make_ladder(range(1, 100), {13: 8, 5: 2}, {})
        self.assertEquals(ladder, [4, 15])

    @mock.patch('random.sample')
    def test_make_invalid_ladder_4(self, choice_mock):
        """Cannot start a ladder where a ladder already begins"""
        choice_mock.side_effect = [[8, 13], [6, 3]]
        ladder = make_ladder(range(1, 100), {}, {8: 16})
        self.assertEquals(ladder, [3, 6])

    @mock.patch('random.sample')
    def test_make_snake(self, choice_mock):
        """Creates a snake and returns in correct order"""
        choice_mock.return_value = [3, 8]
        snake = make_snake(range(1, 100), {})
        self.assertEquals(snake, [8, 3])

    @mock.patch('random.sample')
    def test_make_invalid_snake(self, choice_mock):
        """Creates snake with head that already exists"""
        choice_mock.side_effect = [[3, 8], [5, 14]]
        snake = make_snake(range(1, 100), {8: 4})
        self.assertEquals(snake, [14, 5])

    def test_generate_points(self):
        """Successfully creates a set of snakes and ladders that aren't invalid"""
        snakes, ladders = generate_points(range(1, 100), 24, 23)
        overlap_keys = set(
            list(
                snakes.keys())).intersection(
            list(
                ladders.keys()))
        # For old test that didn't allow chaining of snakes and ladders
        # overlap_key_values = set(list(snakes.keys())).intersection(list(ladders.values()))
        # overlap_value_keys = set(list(snakes.values())).intersection(list(ladders.keys()))
        self.assertEquals(list(overlap_keys), [])
        # Corresponds to comment above
        # self.assertEquals(list(overlap_key_values), [])
        # self.assertEquals(list(overlap_value_keys), [])

        self.assertIsInstance(snakes, dict)
        self.assertIsInstance(ladders, dict)
        self.assertEquals(len(snakes), 24)
        self.assertEquals(len(ladders), 23)

    def test_no_infinite_chains(self):
        """Any infinite loops are caught"""
        snakes, ladders = {24: 13}, {13: 24}
        self.assertFalse(no_loops(snakes, ladders))

        snakes, ladders = {33: 11, 44: 14}, {11: 44, 14: 33}
        self.assertFalse(no_loops(snakes, ladders))

        snakes, ladders = {}, {}
        self.assertTrue(no_loops(snakes, ladders))

        snakes, ladders = {44: 11, 55: 12}, {15: 22, 25: 66}
        self.assertTrue(no_loops(snakes, ladders))
