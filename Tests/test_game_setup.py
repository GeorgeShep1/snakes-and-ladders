# Tests for aspects of game, testing transition matrix, movement, end
# conditions, start conditions

from unittest import TestCase, mock, main
import unittest
import numpy as np
from Sample.Game.game_setup import Game
import inspect
import sys
import os

currentdir = os.path.dirname(
    os.path.abspath(
        inspect.getfile(
            inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)


class BasicTestSuite(unittest.TestCase):
    """Tests setup of board, including start and end conditions, and transition matrix"""

    def setUp(self):
        """Common game for each test"""
        self.game = Game(10)

    def tearDown(self):
        """Destroy game"""
        self.game = None

    def test_empty_transition_0(self):
        """Test creation of empty transition matrix of correct size with start condition of 0"""
        game = Game(10, start_condition=0)
        empty_transition = game.create_empty_transition()
        self.assertEqual(len(empty_transition[0]), 11)

    def test_empty_transition_1(self):
        """Test creation of empty transition matrix with start condition of 1"""
        game = Game(10, start_condition=1)
        empty_transition = game.create_empty_transition()
        self.assertEqual(len(empty_transition[0]), 10)

    def test_starting_vector(self):
        """Test starting vector for both start conditions"""
        game = self.game
        game.start_condition = 0
        starting_vector = game.starting_vector()
        self.assertEqual(len(starting_vector), 11)
        self.assertEqual(starting_vector[0], 1)
        self.assertIsInstance(starting_vector, np.ndarray)

    def test_end_conditions(self):
        """Tests rounding places any square over on the final square"""
        game = self.game
        start_square = 9
        landing_square = 12

        end = game.round_end(start_square, landing_square)
        self.assertEquals(end, game.size)

        end = game.exact_only(start_square, landing_square)
        self.assertEquals(end, start_square)

        end = game.bounce_back(start_square, landing_square)
        self.assertEquals(end, 8)

        game = Game(2)
        end = game.bounce_back(1, 4)
        self.assertEquals(end, 1)


class TransitionTesting(unittest.TestCase):
    """Tests all 6 configurations of start and end conditions"""

    def test_transition_0_bounce(self):
        game = Game(4, end_condition='Bounce', start_condition=0)
        transition = game.transition_matrix(range(0, 5), {3: 2}, {})
        expected = np.array([[0, 0.25, 0.5, 0, 0.25],
                             [0, 0, 0.75, 0, 0.25],
                             [0, 0, 0.75, 0, 0.25],
                             [0, 0.25, 0.5, 0, 0.25],
                             [0, 0, 0, 0, 1]])
        self.assertEquals(transition.tolist(), expected.tolist())

    def test_transition_0_exact(self):
        game = Game(4, end_condition='Exact', start_condition=0)
        transition = game.transition_matrix(range(0, 5), {}, {1: 2})
        expected = np.array([[0, 0, 0.5, 0.25, 0.25],
                             [0, 0, 0.5, 0.25, 0.25],
                             [0, 0, 0.5, 0.25, 0.25],
                             [0, 0, 0, 0.75, 0.25],
                             [0, 0, 0, 0, 1]])
        self.assertEquals(transition.tolist(), expected.tolist())

    def test_transition_0_round(self):
        game = Game(4, end_condition='Over', start_condition=0)
        transition = game.transition_matrix(range(0, 5), {2: 1}, {})
        expected = np.array([[0, 0.5, 0, 0.25, 0.25],
                             [0, 0.25, 0, 0.25, 0.5],
                             [0, 0, 0, 0.25, 0.75],
                             [0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 1]])
        self.assertEquals(transition.tolist(), expected.tolist())

    def test_transition_1_bounce(self):
        game = Game(4, end_condition='Bounce', start_condition=1)
        transition = game.transition_matrix(range(1, 5), {2: 1}, {})
        expected = np.array([[0.25, 0, 0.5, 0.25],
                             [0.25, 0, 0.5, 0.25],
                             [0.5, 0, 0.25, 0.25],
                             [0, 0, 0, 1]])
        self.assertEquals(transition.tolist(), expected.tolist())

    def test_transition_1_exact(self):
        game = Game(4, end_condition='Exact', start_condition=1)
        transition = game.transition_matrix(range(1, 5), {2: 1}, {})
        expected = np.array([[0.5, 0, 0.25, 0.25],
                             [0.5, 0, 0.25, 0.25],
                             [0, 0, 0.75, 0.25],
                             [0, 0, 0, 1]])
        self.assertEquals(transition.tolist(), expected.tolist())

    def test_transition_1_round(self):
        game = Game(4, end_condition='Over', start_condition=1)
        transition = game.transition_matrix(range(1, 5), {2: 1}, {})
        expected = np.array([[0.25, 0, 0.25, 0.5],
                             [0, 0, 0.25, 0.75],
                             [0, 0, 0, 1],
                             [0, 0, 0, 1]])
        self.assertEquals(transition.tolist(), expected.tolist())

    def test_move_0(self):
        """Tests movement of piece after 1 and 2 moves"""
        game = Game(4, end_condition='Over', start_condition=1)
        game.transition_matrix(range(1, 5), {2: 1}, {})
        position = game.move()
        expected_position = np.array([0.25, 0, 0.25, 0.5])
        self.assertEquals(position.tolist(), expected_position.tolist())
        self.assertEquals(position.sum(), 1)

        position = game.move()
        expected_position = np.array(
            [0.25 * 0.25, 0, 0.25 * 0.25, 0.75 + 0.25 * 0.5])
        self.assertEquals(position.tolist(), expected_position.tolist())
        self.assertEquals(position.sum(), 1)

    def test_chain_snakes_ladders(self):
        """Tests transition matrix correctly deals with snakes and ladders chained together"""
        game = Game(6, end_condition='Over', start_condition=1)
        game.transition_matrix(range(1, 7), {3: 1}, {2: 3})
        position = game.move()
        expected_position = np.array([0.5, 0, 0, 0.25, 0.25, 0])
        self.assertEquals(position.tolist(), expected_position.tolist())
        self.assertEquals(position.sum(), 1)

    def test_chain_snakes_ladders_2(self):
        """Tests transition matrix handles a large amount of snakes and ladders chained together"""
        game = Game(8, end_condition='Over', start_condition=1)
        game.transition_matrix(
            range(
                1, 9), {
                2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1}, {
                1: 8})
        position = game.move()
        expected_position = np.array([0, 0, 0, 0, 0, 0, 0, 1])
        self.assertEquals(position.tolist(), expected_position.tolist())
        self.assertEquals(position.sum(), 1)
