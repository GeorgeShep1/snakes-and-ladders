format:
	autopep8 -r --in-place --aggressive Sample
	autopep8 -r --in-place --aggressive Scripts
	autopep8 -r --in-place --aggressive Tests
test:
	python3 Scripts/test.py

